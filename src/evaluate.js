function evaluate (x){
    x = x.replace(/ /g, "") + ")";
    function primary() {
        if (x[0] === '(') {
            x = x.substr(1);
            return expression();
        }

        var n = /^[-+]?\d*\.?\d*/.exec(x)[0];
        x = x.substr(n.length);
        return +n;
    }

    function expression() {
        var a = primary();
        for (;;) {
            var operator = x[0];
            x = x.substr(1);

            if (operator === ')') {
                return a;
            }

            var b = primary();
            a = (operator === '+') ? a + b :
                (operator === '-') ? a - b :
                (operator === '*') ? a * b :
                                    a / b;
        }
    }

    return expression();
}

export default evaluate