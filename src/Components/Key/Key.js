import React, {Component} from 'react';
import PropTypes from "prop-types"
import './Key.scss';

const propTypes = {
    text: PropTypes.string,
    updateInput: PropTypes.func,
    customClass: PropTypes.string
};

const Key = ({text, updateInput, customClass}) => {
    let defaultClass = "number-key";
    if (customClass) defaultClass += " " + customClass;

    return (
        <div className="col-3">
            <div className={defaultClass} onClick={() => updateInput ? updateInput(text) : ""}>
                {text}
            </div>
        </div>
    );
};

Key.propTypes = propTypes;

export default Key;
