import React, {Component} from 'react';
import Key from "./Components/Key/Key"
import './App.scss';
import evaluate from './evaluate';

class App extends Component {
    constructor() {
        super();
        this.state = {
            inputValue: "",
            result: 0
        }

        this.operations = ["*", "-", "+", "/"];
        this.duplicateNotAllowed = ["."];
    }

    updateInput(inputValue) {
        let currentValue = this.state.inputValue;
        let result = this.state.result;
        let lastCharacter = currentValue[currentValue.length - 1];

        if (this.operations.indexOf(lastCharacter) !== -1 && this.operations.indexOf(inputValue) !== -1) {
            console.log("Warning! Duplicate operation not allowed")
            return;
        }

        if (lastCharacter === inputValue && this.duplicateNotAllowed.indexOf(lastCharacter) !== -1) {
            console.log("Duplicate character, ", lastCharacter);
            return;
        }

        let secondLastCharacter = currentValue[currentValue.length - 2];
        if ((this.operations.indexOf(secondLastCharacter) !== -1 && lastCharacter === "0") && inputValue !== ".") {
            currentValue = currentValue.slice(0, -1);
        }

        switch (inputValue) {
            case "C":
                currentValue = currentValue.slice(0, -1);
                break;

            case "A":
                currentValue = "";
                result = 0;
                break;

            default:
                currentValue = currentValue + inputValue.toString()
        }

        this.setState({
            inputValue: currentValue,
            result: result
        })
    }

    calculatePercentage() {
        let currentValue = this.state.inputValue;
        let result = this.state.result;

        let value = currentValue.match(/([^\+\*\-\/]*$)/);
        let percentageValue = parseFloat(value ? value[0] : 0);

        if (!percentageValue) return;

        let remainingExpression = currentValue.slice(0, value.index);

        var total = evaluate(remainingExpression.slice(0, -1));
        var percentage = total * (percentageValue / 100)

        let finalStringToEvaluate = total.toString() + remainingExpression[remainingExpression.length - 1] + percentage;
        result = evaluate(finalStringToEvaluate);

        if (isNaN(result)) return;

        this.setState({
            result: result
        })
    }

    evaluateInput() {

        // A simple way of doing it would be just cheating and using eval 
        // but its not considered safe due to security concerns
        // let result = eval(this.state.inputValue);


        let result = evaluate(this.state.inputValue);

        if (isNaN(result)) result = 0;

        this.setState({
            result: result
        });
    }

    render() {
        const {inputValue, result} = this.state;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-6 offset-3">
                        <div className="calc-wrapper">
                            <div className="result-wrapper">
                                {result}
                            </div>
                            <div className="input-wrapper">
                                {inputValue}
                            </div>
                            <div className="calc-keys">
                                <div className="row">
                                    <Key text={"A"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"C"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"%"} updateInput={this.calculatePercentage.bind(this)}/>
                                    <Key customClass="operation" text={"/"} updateInput={this.updateInput.bind(this)}/>
                                </div>
                                <div className="row">
                                    <Key text={"7"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"8"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"9"} updateInput={this.updateInput.bind(this)}/>
                                    <Key customClass="operation" text={"*"} updateInput={this.updateInput.bind(this)}/>
                                </div>
                                <div className="row">
                                    <Key text={"4"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"5"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"6"} updateInput={this.updateInput.bind(this)}/>
                                    <Key customClass="operation" text={"-"} updateInput={this.updateInput.bind(this)}/>
                                </div>
                                <div className="row">
                                    <Key text={"1"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"2"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"3"} updateInput={this.updateInput.bind(this)}/>
                                    <Key customClass="operation" text={"+"} updateInput={this.updateInput.bind(this)}/>
                                </div>
                                <div className="row">
                                    <Key text={"0"} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={"("} updateInput={this.updateInput.bind(this)}/>
                                    <Key text={")"} updateInput={this.updateInput.bind(this)}/>
                                    <Key customClass="equal" text={"="} updateInput={this.evaluateInput.bind(this)}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
